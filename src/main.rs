#[macro_use] extern crate serde_derive;
#[macro_use] extern crate lazy_static;

use opcua_client::prelude::*;
use std::env;

lazy_static!{
    static ref OPC: String = env::var("OPC").unwrap_or("".to_string());
    static ref SOCKET: String = env::var("SOCKET").unwrap_or("".to_string());
    static ref INFLUX: String = env::var("INFLUX").unwrap_or("".to_string());
}

fn main() {
    let mut all_set = true;
    if OPC.is_empty() {
        eprintln!("Please set the env OPC to the OPC server");
        all_set = false;
    }
    if SOCKET.is_empty() {
        eprintln!("Please set the env SOCKET to the socket server");
        all_set = false;
    }
    if INFLUX.is_empty() {
        eprintln!("Please set the env INFLUX to the influx server");
        all_set = false;
    }
    if !all_set {
        return;
    }
    let builder = ClientBuilder::new()
        .application_name("OPC UA Client")
        .application_uri("urn:Client")
        .pki_dir("./pki");
    let mut client = match builder.client() {
        Some(client) => client,
        None => {
            eprintln!("Client could not be created. Aborting");
            return;
        }
    };
    let session = match client.connect_to_endpoint((&*OPC.as_ref(), SecurityPolicy::Basic256Sha256.to_str(), MessageSecurityMode::SignAndEncrypt, UserTokenPolicy::anonymous()), IdentityToken::Anonymous) {
        Ok(session) => session,
        Err(e) => {
            eprintln!("Session could not be created. Aborting");
            eprintln!("Reason: {}", e);
            return;
        },
    };
    if let Err(e) = sub(session.clone()) {
        eprintln!("Could not add listener to Session. Aborting");
        eprintln!("Reason: {}", e);
        return;
    }
    Session::run(session);
}

fn sub(session: std::sync::Arc<std::sync::RwLock<Session>>) -> Result<(), StatusCode> {
    let mut session = session.write().map_err(|_| StatusCode::BadNoSubscription)?;
    let subscript = session.create_subscription(500.0, 2400, 10, 0, 0, true, DataChangeCallback::new(|changed| {
        changed.iter().for_each(|item| parse_and_send_event(item));
    }))?;
    let items: Vec<MonitoredItemCreateRequest> = (2..=6).map(|v| ReadValueId::from(NodeId::new(2,Identifier::Numeric(v)))).map(|node| MonitoredItemCreateRequest::new(node, MonitoringMode::Reporting, MonitoringParameters::default())).collect();
    session.create_monitored_items(subscript, TimestampsToReturn::Both, &items)?;
    Ok(())
}

fn parse_and_send_event(item: &MonitoredItem) {
    let event = match (&item.item_to_monitor().node_id.identifier, &item.value().value) {
        (Identifier::Numeric(2), Some(Variant::String(datetime))) => Some(Events::TimeStamp(NewDateTime{machine_id: 12, datetime: chrono::NaiveDateTime::parse_from_str(&datetime.to_string(),"%Y-%m-%d %H:%M:%S").unwrap()})),
        (Identifier::Numeric(3), Some(Variant::Int64(val))) => Some(Events::Status(UpdateStatus{machine_id: 12, ty: StatusType::Run, status: *val == 1})),
        (Identifier::Numeric(4), Some(Variant::Int64(val))) => Some(Events::Status(UpdateStatus{machine_id: 12, ty: StatusType::Auto, status: *val == 1})),
        (Identifier::Numeric(5), Some(Variant::Int64(val))) => Some(Events::Status(UpdateStatus{machine_id: 12, ty: StatusType::Error, status: *val == 1})),
        (Identifier::Numeric(6), Some(Variant::Int64(count))) => Some(Events::Counter(UpdateEventCounter{machine_id: 12, count: *count})),
        _ => None,
    };
    event.map(|event| send_event(event));
}

fn send_event(event: Events) {
    let client = reqwest::Client::new();
    let influx = format!("{}",*INFLUX);
    let socket = format!("{}",*SOCKET);
    let (influx_error, socket_error) = match event {
        Events::TimeStamp(t) => {
           (client.post(&(influx + "/datetime")).json(&t).send(), client.post(&(socket + "/datetime")).json(&t).send())
        },
        Events::Status(s) => {
            (client.put(&(influx + "/status")).json(&s).send(), client.post(&(socket + "/status")).json(&s).send())
        },
        Events::Counter(c) => {
            (client.put(&(influx + "/eventcounter")).json(&c).send(), client.post(&(socket +"eventcounter")).json(&c).send())
        },
    };
    match influx_error {
        Ok(response) => {
            if !response.status().is_success() {
                eprintln!("Influx server error: {:?}", response);
            }
        },
        Err(e) => eprintln!("{}",e),
    }
    match socket_error {
        Ok(response) => {
            if !response.status().is_success() {
                eprintln!("Socket server error: {:?}", response);
            }
        },
        Err(e) => eprintln!("{}",e),
    }
}


#[derive(Debug, Serialize)]
enum StatusType {
    Auto,
    Error,
    Run,
}

#[derive(Debug, Serialize)]
struct UpdateStatus {
    machine_id: i64,
    ty: StatusType,
    status: bool,
}


#[derive(Debug, Serialize)]
struct NewDateTime {
    machine_id: i64,
    datetime: chrono::NaiveDateTime,
}
#[derive(Serialize, Debug)]
struct UpdateEventCounter {
    machine_id: i64,
    count: i64,
}

#[derive(Debug)]
enum Events {
    Status(UpdateStatus),
    TimeStamp(NewDateTime),
    Counter(UpdateEventCounter),
}

impl serde::ser::Serialize for Events {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> 
    where S: serde::ser::Serializer {
        match self {
            Self::Status(s) => s.serialize(serializer),
            Self::TimeStamp(t) => t.serialize(serializer),
            Self::Counter(c) => c.serialize(serializer),
        }
    }
}
